module gitlab.com/tslocum/netris

go 1.13

require (
	github.com/anmitsu/go-shlex v0.0.0-20200514113438-38f4b401e2be // indirect
	github.com/creack/pty v1.1.11
	github.com/gdamore/tcell/v2 v2.0.0-dev.0.20200926152101-0fb77ddaa5b4
	github.com/gliderlabs/ssh v0.3.1
	github.com/mattn/go-isatty v0.0.12
	gitlab.com/tslocum/cbind v0.1.2
	gitlab.com/tslocum/cview v1.5.1-0.20201007233521-ed5e6d94dd16
	golang.org/x/crypto v0.0.0-20201002170205-7f63de1d35b0
	golang.org/x/sys v0.0.0-20201007165808-a893ed343c85 // indirect
	gopkg.in/yaml.v2 v2.3.0
)
